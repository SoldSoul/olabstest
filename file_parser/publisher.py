import ftplib
from file_parser.rabbitmq import Publisher


class CSVPublisher(Publisher):
    host = None
    user = None
    password = None
    path = None

    def __init__(self, app_id, config, options):
        self.host = options.host
        self.user = options.user
        self.password = options.password
        if options.directory:
            self.path = options.directory if options.directory[:1] == '/' else '/' + options.directory
        super(CSVPublisher, self).__init__(app_id, config)

    def run(self):
        self._connection = self.connect()
        ftp = ftplib.FTP(self.host, self.user, self.password)

        if self.path:
            try:
                ftp.cwd(self.path)
            except ftplib.all_errors, resp:
                if str(resp) == '550 The system cannot find the file specified. ':
                    print 'Directory tot found'
                    return
                else:
                    raise

        try:
            ftp.retrlines('NLST', callback=self.handle_line)
        except ftplib.error_perm, resp:
            if str(resp) == '550 No files found':
                print 'No files in this directory'
            else:
                raise

    def handle_line(self, file_name):
        if file_name[-4:] == '.csv':
            self.publish_message(
                {
                    'host': self.host,
                    'user': self.user,
                    'password': self.password,
                    'path': self.path,
                    'file_name': file_name
                }
            )
