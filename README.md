FTP CSV-file parser
===================

Get files list:
---------------

``` shell
$ python start.py -h HOST [-u USER] [-p PASSWORD] [-d DIRECTORY]
```

Parse files consumer:
---------------------
``` shell
$ python parser.py
```
