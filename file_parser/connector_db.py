from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def create_db_connection(config, db):
    """
    :param dict config:
    :param sqlalchemy.ext.declarative.declarative_base db:
    :return: sqlalchemy.orm.Session
    """
    connection_string = config['type'] + '://'

    if config['user']:
        connection_string += config['user']
        if config['password']:
            connection_string += ':' + config['password']
        connection_string += '@'

    connection_string += config['host']

    if config['port']:
        connection_string += ':' + str(config['port'])

    connection_string += '/' + config['name']

    engine = create_engine(connection_string, encoding=config['charset'])

    db.metadata.bind = engine

    session = sessionmaker(bind=engine)

    return session()
