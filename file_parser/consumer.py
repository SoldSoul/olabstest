import csv
import ftplib
import json
import logging
from tempfile import NamedTemporaryFile

from .rabbitmq import Consumer
from .model import Order

LOGGER = logging.getLogger(__name__)


class CSVParseConsumer(Consumer):
    session = None

    row_indexes = {
        'orderId': 0,
        'productId': 1,
        'productPrice': 2,
        'productCount': 3,
        'date': 4
    }

    def __init__(self, config, session):
        self.session = session
        super(CSVParseConsumer, self).__init__(config)

    def on_message(self, unused_channel, basic_deliver, properties, body):
        try:
            message = json.loads(body)
        except Exception as e:
            LOGGER.warn('Unknown received message format: %s' % str(e))
            self.nack_message(basic_deliver.delivery_tag, requeue=False)
            return

        if all(k in message for k in ('host', 'user', 'password', 'path', 'file_name')):
            ftp = ftplib.FTP(message['host'], message['user'], message['password'])

            tmp_file = NamedTemporaryFile(suffix='.csv')

            try:
                if message['path']:
                    ftp.cwd(message['path'])
                ftp.retrlines('RETR %s' % message['file_name'], lambda s, w=tmp_file.file.write: w(s+'\n'))
                tmp_file.seek(0)
                csv_data = csv.reader(tmp_file)
            except Exception as e:
                LOGGER.error(str(e))
                self.nack_message(basic_deliver.delivery_tag, requeue=False)
                return

            for line in csv_data:
                if self._validate_line(line):
                    self._save_order(line)

            tmp_file.close()

            self.acknowledge_message(basic_deliver.delivery_tag)
        else:
            LOGGER.warn('Unknown received message format: %s' % str(body))
            self.nack_message(basic_deliver.delivery_tag, requeue=False)

    def _validate_line(self, line):
        try:
            assert line[self.row_indexes['orderId']]

            assert int(line[self.row_indexes['productId']]) > 0

            assert float(line[self.row_indexes['productPrice']]) > 0.0

            assert int(line[self.row_indexes['productCount']]) > 1

            assert line[self.row_indexes['date']]
        except Exception as e:
            LOGGER.warn('Validation error: %s' % str(e))
            return False

        return True

    def _save_order(self, line):
        order = Order()
        order.order_id = line[self.row_indexes['orderId']]
        order.product_id = int(line[self.row_indexes['productId']])
        order.product_price = float(line[self.row_indexes['productPrice']])
        order.product_count = int(line[self.row_indexes['productCount']])
        order.created_at = line[self.row_indexes['date']]

        self.session.add(order)
        self.session.commit()
