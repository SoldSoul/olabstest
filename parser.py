import logging
import os
from file_parser import CSVParseConsumer, create_db_connection, db, get_config


LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)


if __name__ == '__main__':
    config = get_config(os.path.abspath(os.path.dirname(__file__)) + '/file_parser.conf')

    session = create_db_connection(config['DB'], db)

    consumer = CSVParseConsumer(config['RabbitMQ'], session)
    consumer.run()
