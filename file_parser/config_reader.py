def get_config(config_path, sections=None):
    """
    :param str config_path:
    :param list sections:
    :return ConfigParser:
    """
    import os
    from ConfigParser import ConfigParser

    if not sections:
        sections = ['DB', 'RabbitMQ']

    config = {}
    config_parser = ConfigParser()
    if os.path.isfile(config_path):
        config_parser.read(config_path)

        for section in sections:
            if config_parser.has_section(section):
                config[section] = {}
                for key in config_parser.options(section):
                    config[section][key] = config_parser.get(section, key)
            else:
                ValueError('Section %s not found in config.' % section)
        return config
    else:
        raise ValueError('Config not found')
