import logging
import os
from file_parser import get_config, CSVPublisher


LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)


def init_options():
    import argparse

    parser = argparse.ArgumentParser(description='FTP files publisher.', add_help=False)
    parser.add_argument(
        '-h', '--host', action='store', dest='host', help='FTP server', type=str, required=True
    )
    parser.add_argument(
        '-u', '--user', action='store', dest='user', help='FTP username', type=str
    )
    parser.add_argument(
        '-p', '--password', action='store', dest='password', help='FTP password', type=str
    )
    parser.add_argument(
        '-d', '--directory', action='store', dest='directory', help='Directory path', type=str
    )

    return parser.parse_args()


if __name__ == '__main__':
    options = init_options()
    config = get_config(os.path.abspath(os.path.dirname(__file__)) + '/file_parser.conf', ['RabbitMQ'])
    publisher = CSVPublisher('ftp-csv-publisher', config['RabbitMQ'], options)
    publisher.run()
