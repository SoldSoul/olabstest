from sqlalchemy import Column, Integer, Float, String, Sequence
from sqlalchemy.ext.declarative import declarative_base

db = declarative_base()


class Order(db):
    __tablename__ = 'order'
    id = Column('id', Integer(), Sequence('order_id_seq'), primary_key=True, nullable=False)
    order_id = Column('order_id', Integer(), nullable=False)
    product_id = Column('product_id', Integer(), nullable=False)
    product_price = Column('product_price', Float(), nullable=False)
    product_count = Column('product_count', Float(), nullable=False)
    created_at = Column('created_at', String(length=255), nullable=False)
